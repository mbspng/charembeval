if [ -e "$HOME/miniconda3" ]; then
        . $HOME/miniconda3/etc/profile.d/conda.sh
elif  [ -e "$HOME/anaconda3" ]; then

        . $HOME/anaconda3/etc/profile.d/conda.sh
else
        echo "Expected installation of annaconda in $HOME, found none. If anaconda is installed enter the path to its directory:"
        read anaconda_path
        . $anaconda_path/etc/profile.d/conda.sh
fi


conda env create -f env.yml
conda activate charembeval
./make_dirs.py

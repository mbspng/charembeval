# CharEmbEval

This repository contains code used for research on the effects of different designs of character embeddings on prediction quality in text classification with temporal convolutional neural networks. Specifically: 

- Implementation of a character-level CNN ([Xiang Zhang, Junbo Zhao & Yann LeCun, 2015](https://arxiv.org/abs/1509.01626))
- Generation of different types of character-level embeddings
- Evaluation of said embeddings

# Setup (Linux/macOS)
To obtain the source files and set up the environment navigate to any intended parent directory on your system and run:
```
git clone --depth=1 git@gitlab.com:mbspng/charembeval.git
cd charembeval
./setup.sh
```
This requires a working installation of [Anaconda](https://www.anaconda.com/distribution/).



# Usage

Activate the environment with `conda activate charembeval`. For the Jupyter-notebooks run `jupyter-notebook` and run them in your browser.

First construct a training data set with *data-wrangling.ipynb*, then generate character embeddings with *char_embedding_generation.ipynb*. Next train a model with *train.py*. Finally you can run evaluations with *evaluation.ipynb*.
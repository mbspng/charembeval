from scipy import stats
import numpy as np

###
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
# https://github.com/vyraun/Half-Size/blob/master/algo.py
def ppa_step(X, n_components):

    pca = PCA(n_components=n_components)
    scaler = StandardScaler()
    X_ = scaler.fit_transform(X)
    _ = pca.fit_transform(X_)
    V = pca.components_

    z = []

    # Removing Projections on Top Components
    for i, x in enumerate(X_):
        for u in V[0:7]:
            x = x - np.dot(u.transpose(), x) * u
        z.append(x)

    return np.asarray(z)

def pca_step(X, n_components):

    # PCA Dim Reduction
    pca = PCA(n_components=n_components)
    scaler = StandardScaler()
    X_ = scaler.fit_transform(X)
    return pca.fit_transform(X_)

def raunaks_algorithm(X, n1=300, n2=150):
    z = ppa_step(X, n1)
    z = pca_step(z, n2)
    z = ppa_step(z, n2)
    return z
###

def cohens_kappa(table):
        
    num_items = np.sum(table)
    
    marginals_true = np.sum(table, axis=0)
    marginals_cls = np.sum(table, axis=1)
    
    obs_acc = np.trace(table)/num_items
    exp_acc = np.sum((marginals_true*marginals_cls)/num_items)/num_items
    
    return (obs_acc - exp_acc)/(1 - exp_acc)

def chi_squared_gof(observed_distribution, expected_distribution):
    
    assert(len(expected_distribution) == len(observed_distribution))
    
    expected_counts = np.bincount(expected_distribution)
    observed_counts = np.bincount(observed_distribution)
    
    df = len(expected_counts) - 1
    statistic = np.sum((observed_counts-expected_counts)**2/expected_counts)
    pvalue = stats.chi2.sf(statistic, df)
    
    return DotDict({'pvalue' : pvalue, 'statistic' : statistic, 'df' : df}) 

class DotDict(dict):
    __getattr__ = dict.__getitem__
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__

    def __init__(self, dct):
        for key, value in dct.items():
            if hasattr(value, 'keys'):
                value = DotDict(value)
            self[key] = value

def div0( a, b ):
    with np.errstate(divide='ignore', invalid='ignore'):
        c = np.true_divide( a, b )
        c[ ~ np.isfinite( c )] = 0  # -inf inf NaN
    return c

def mcnemar_bowker(table):
    """
    Computes McNemar-Bowker statistic
    """
    num_cols, num_rows = table.shape
    tril = np.tril(table, k=-1)
    triu = np.triu(table, k=1).T
    
    numer = (tril - triu)**2
    denom = (tril + triu)
    quot = div0(numer, denom)
    
    statistic = np.sum(quot)
    df = int(num_rows * (num_rows -1) / 2)
    pvalue = stats.chi2.sf(statistic, df)
    
    return DotDict({'pvalue' : pvalue, 'statistic' : statistic, 'df' : df})

def construct_confusion_matrix(classifactions_1, classifactions_2):
    
    assert(len(classifactions_1) == len(classifactions_2))
    
    num_cats = len(np.unique(np.concatenate([classifactions_1, classifactions_2])))
    
    table = np.zeros(shape=(num_cats, num_cats))
    
    for c1, c2 in zip(classifactions_1, classifactions_2):
        table[c2, c1]+=1
        
    return table
        
def brier_multi(targets, probs):
    """
    Computes multiclass Brier score.
    targets: binary matrix
    probs: matrix of probabilities, rows summing to one
    """
    assert(targets.shape == probs.shape)
    return np.mean(np.sum((probs - targets)**2, axis=1))
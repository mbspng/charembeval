from keras.utils import Sequence
import pandas as pd
import numpy as np
from IPython.display import display, HTML


class AmazonReviewDataGenerator(Sequence):
    def __init__(self,
                 data: pd.DataFrame,
                 text_length: int,
                 X_encoder,
                 Y_encoder,
                 random_span=False,
                 batch_size: int = 128,
                 shuffle=True):
        """
        data generator for the amazon review data. expects fields `summary`, `text`, `category`
        data : data frame containing items to feed to network
        batch_size : batch size
        text_length : length for text (amazon review text) of items
        random_span : whether to (if even possible) select the text as a random
                      span from the base text (of length $text_lenghth) or to start
                      from the beginning of the text
        """
        self.data = data
        self.X_encoder = X_encoder
        self.Y_encoder = Y_encoder
        self.batch_size = batch_size
        self.length = len(self.data)//self.batch_size
        self.shuffle = shuffle
        
        if self.shuffle:
            self.shuffle_data()
        
    def shuffle_data(self):
        self.data = self.data.sample(frac=1).reset_index(drop=True)

    def on_epoch_end(self):
        if self.shuffle:
            self.shuffle_data()

    def __len__(self):
        return self.length

    def __getitem__(self, idx):
        batch_items = self.data.iloc[idx * self.batch_size:idx * self.batch_size+self.batch_size]
        
        X = (batch_items.summary + ' :: ' + batch_items.text).values
        X = list(map(self.X_encoder, X))
        

        # for word embedding layer
        X = np.vstack(X)
        
        Y = batch_items.category.cat.codes.values
        Y = self.Y_encoder(Y)
        Y = np.transpose(Y)

        return X, Y
import time
from keras.callbacks import Callback
import warnings

class BatchHistory(Callback):
    
    def __iadd__(self, rhs):
        self.logs['acc']+=(rhs.logs['acc'])
        self.logs['loss']+=(rhs.logs['loss'])
        self.logs['val_acc']+=(rhs.logs['val_acc'])
        self.logs['val_loss']+=(rhs.logs['val_loss'])
        self.logs['epoch_cnt']+=rhs.logs['epoch_cnt']
        self.logs['epoch_ends']+=list(map(lambda x : x + self.logs['epoch_ends'][-1],
                                          rhs.logs['epoch_ends']))
        return self
    
    def __init__(self):
        super().__init__()
        self.logs =  {'loss' : [],
                      'acc' : [],
                      'val_acc' : [],
                      'val_loss' : [],
                      'epoch_cnt' : 0,
                      'epoch_ends' : [],
                      'time_elapsed' : 0, # seconds
                      'epoch_sizes' : []
                     }
        self.start_time = time.time() 
        self.epoch_size__ = 0
        
    def on_train_begin(self, logs={}):
        pass
    
    def truncate_trailing_batches(self):
        # cut off datapoints of possibly incomplete last epoch:
        epoch_ends = self.logs['epoch_ends']
        num_epochs = self.logs['epoch_cnt']
        epoch_size = epoch_ends[0]
        cutoff = num_epochs*epoch_size 

        if cutoff < len(self.logs['acc']):
            print(f'cutting off {len(self.logs["acc"])} - cutoff batches') 
            self.logs['acc']  = self.logs['acc'][:cutoff]
            self.logs['loss'] = self.logs['loss'][:cutoff]
    
    def fix_epochs__(self):
    
        from math import ceil

        epoch_ends = self.logs['epoch_ends']
        epoch_size = epoch_ends[0]

        num_epochs = ceil(epoch_ends[-1]/epoch_size)
        
        epoch_ends_fixed = [epoch_size * i for i in range(1, num_epochs+1)]

        if (epoch_ends_fixed[-1] != epoch_ends[-1]):
            warnings.warn(f'new index for last epoch end does not match prior index. difference: {epoch_ends_fixed[-1] != epoch_ends[-1]}')
        
        self.logs['epoch_ends'] = epoch_ends_fixed 
        self.logs['epoch_cnt'] = num_epochs
        
        
    def fix_val_acc__(self):
        
        from math import ceil
        import numpy as np
        
        val_accs = self.logs['val_acc']
        epoch_ends = self.logs['epoch_ends']
        
        epoch_size = epoch_ends[0]
        val_accs_fixed = []

        for i, (e1, e2) in enumerate(zip(epoch_ends[:-1], epoch_ends[1:])):
            missing = ceil((e2-e1)/epoch_size)-1
            if missing == 0:
                val_accs_fixed.append(val_accs[i])
            else:
                val_accs_fixed.append(val_accs[i])
                for _ in range(missing):
                    val_accs_fixed.append(np.mean((val_accs_fixed[-1], val_accs[i+1])))

        val_accs_fixed.append(val_accs[-1])
        self.logs['val_acc'] = val_accs_fixed
        
        
    def fix(self):
        """
        if traing was interrupted, logs will be corrupted. This method fixes broken logs
        through averaging techniques.
        """
        self.fix_val_acc__()
        self.fix_epochs__()
        

    def on_batch_end(self, batch, logs={}):
        self.logs['acc'].append(logs.get('acc'))
        self.logs['loss'].append(logs.get('loss'))
        self.logs['time_elapsed']=int(time.time()-self.start_time)
        self.epoch_size__+=1
        
    def on_epoch_end(self, epochs, logs=None):
        self.logs['epoch_cnt']+=1
        self.logs['epoch_ends'].append(len(self.logs['loss']))
        self.logs['val_acc'].append(logs.get('val_acc'))
        self.logs['val_loss'].append(logs.get('val_loss'))
        if len(self.logs['epoch_sizes']) > 0 and self.epoch_size__ != self.logs['epoch_sizes'][0]:
            warnings.warn(f'detected varying epoch sizes. difference: {self.epoch_size__ - self.logs["epoch_sizes"][0]} batches')
        self.logs['epoch_sizes'].append(self.epoch_size__)
        self.epoch_size__ = 0
from time import sleep, time
import numpy as np


class ETADisplay:
    def __init__(self, max_steps, start_time=time()):
        self.start_time = start_time
        self.max_steps = max_steps
        self.elapsed = 0
        self.eta = -1
        self.step = 1

# public
    def update(self, increment=1):
        self.elapsed = self.get_elapsed()
        self.eta = self.get_eta()
        self.step += increment

    def get_eta_display(self):
        return self.get_display(self.eta)

    def get_elapsed_display(self):
        return self.get_display(self.elapsed)

    def get_joined_display(self):
        eta = self.get_eta_display()
        elapsed = self.get_elapsed_display()
        return f'ETA: {eta} | elapsed: {elapsed} | {np.round(100*self.step/self.max_steps, 2):05} %'

    def display(self):
        print(self.get_joined_display(), end='\r')

# private
    def hms(self, t):
        hours, rem = divmod(t, 3600)
        minutes, seconds = divmod(rem, 60)
        return hours, minutes, seconds

    def get_display(self, t):
        hours, minutes, seconds = self.hms(t)
        return "{:0>2}:{:0>2}:{:0>2}".format(int(hours), int(minutes), int(seconds))

    def get_eta(self):
        return (self.max_steps/self.step)*self.elapsed - self.elapsed

    def get_elapsed(self):
        return time() - self.start_time
    
    
def eta_decorator(num_calls, interval=1000, message='items processed'):
    """
    num_calls : number of calls after which the process to give an ETA for will terminate
    intervall : number of calls to update ETA display after
    """
    
    def inner_decorator(func):

        eta_display = ETADisplay(num_calls)
        current = 0

        def inner(*args, **kwargs):

            nonlocal current
            current += 1

            if not current % interval:

                eta_display.update(interval)

                print(f"{current} {message} | {eta_display.get_joined_display()}", end='\r')

            return func(*args, **kwargs)

        return inner
    
    return inner_decorator


if __name__ == '__main__':

    def f():
        sleep(1)

    n = 60
    fs = (f for _ in range(n))

    eta_display = ETADisplay(n)
    for f in fs:
        f()
        eta_display.update()
        eta_display.display()

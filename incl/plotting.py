import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from matplotlib.ticker import MultipleLocator, FormatStrFormatter
from matplotlib.font_manager import FontProperties
import numpy as np
from sklearn.decomposition import PCA
import sklearn.preprocessing

import brewer2mpl
# brewer2mpl.get_map args: set name  set type  number of colors
bmap = brewer2mpl.get_map('Set2', 'qualitative', 7)
colors = bmap.mpl_colors

linestyles = ['-', '--', ':', '=.']

####
# import matplotlib as mpl
# mpl.use("pgf")
# import matplotlib.pyplot as plt
# plt.rcParams.update({
#     "font.family": "serif",  # use serif/main font for text elements
#     "text.usetex": True,     # use inline math for ticks
#     "pgf.rcfonts": False,    # don't setup fonts from rc parameters
# })
####

def plot_gallery(plotting_function, data, *args):
    
    args = [data]+list(args)
    l1 = list(range(0,len(data),2))
    l2 = list(range(1,len(data),2))
    idcs = l1+l2

    
    n_col = 2
    n_row = len(data)//n_col
    fig, axes = plt.subplots(n_row, n_col)
    fig.set_size_inches(n_col*5, n_row*3)

    for i, idx in enumerate(idcs):
        ax = plt.subplot(n_row, n_col, idx+1)
        fargs = list(map(lambda arg : arg[i], args))
        plotting_function(*fargs, ax)


def plot_history(history, caption=None, ax = None):
    
    def interpolate(y, box_pts=800):
        box = np.ones(box_pts)/box_pts
        y_smooth = np.convolve(y, box, mode='same')
        return y_smooth
    
    logs = history.logs
    
    epoch_ends = logs['epoch_ends']
    num_epochs = logs['epoch_cnt']
    epoch_size = epoch_ends[0]
    cutoff = num_epochs*epoch_size # cut off datapoints of possibly incomplete last epoch
    acc = logs['acc'][:cutoff]
    loss = logs['loss'][:cutoff]
    batches = range(1, len(acc)+1)
    
    iws = len(batches)//50+1 # interpolation window size
    interp_xs = batches[iws:-iws]
    
    lns = []
    
    if ax is None:
        fig, ax = plt.subplots( nrows=1, ncols=1 )
    else:
        fig = ax.figure
    ax2 = ax.twinx()
    ax3 = ax.twiny()
    ax.set_xlim(0, len(batches))
    ax3.set_xlim(0, len(logs['epoch_ends']))
    
    ax.set_xlabel('batches')
    ax.set_ylabel('accuracy')
    ax2.set_ylabel('loss')
    ax3.set_xlabel('epochs')
    ax.grid(linestyle='--', alpha=.5)
    ax3.grid(linestyle='--', alpha=.5)
    ax.xaxis.grid(False)
    ax3.yaxis.grid(False)

    acc_interpolated = interpolate(acc, iws)[iws:-iws]
    lns += ax.plot(interp_xs, acc_interpolated, color='k', linestyle=linestyles[0], linewidth=.7, label='acc')

    lns += ax.plot(logs['epoch_ends'], logs['val_acc'], color='k', linestyle=linestyles[2], label='val acc')
    
    loss_interpolated = interpolate(loss, iws)[iws:-iws]
    lns += ax2.plot(interp_xs, loss_interpolated, color='k', linestyle=linestyles[1], dashes=(5, 10), linewidth=.9, label='loss')

    labs = [l.get_label() for l in lns]
    fontP = FontProperties()
    fontP.set_size('medium')
    fig.legend(lns, labs, loc='upper center', bbox_to_anchor=(.5, .04),
               fancybox=True, shadow=True, ncol=4, prop=fontP)
    fig.tight_layout()
    
    if caption:
        ax.set_title(caption)
    
    return fig
    

def signifiance_map2(p_values, labels, alphas):
    
    cmap = plt.cm.inferno
    
    colors = cmap(np.linspace(0, 1, len(alphas)))
    
    bitmap = np.vstack(np.array(list(p_values)))
    
    fig, ax = plt.subplots()
        
    ax.xaxis.set_ticks_position('none')
    ax.set_xticks(range(len(labels)))
    ax.set_xticks(np.arange(.5, len(labels)+.5), minor=True)
    ax.set_xticklabels(labels, rotation=90, fontsize=7)
    
    ax.yaxis.set_ticks_position('none')
    ax.set_yticks(range(len(labels)))
    ax.set_yticks(np.arange(.5, len(labels)+.5), minor=True)
    ax.set_yticklabels(labels, rotation=0, fontsize=7)
    
    ax.grid(which='minor', linestyle='dotted') 
#     cax = ax.imshow(bitmap, interpolation='nearest', cmap=cmap,
#                     vmin=np.min(bitmap), vmax=np.max(bitmap))
    cax = ax.imshow(bitmap, interpolation='nearest', cmap=cmap)
    
    line1 = Line2D(range(1), range(1), color="white", marker='s', markerfacecolor='white', markeredgecolor='grey')
    line2 = Line2D(range(1), range(1), color="white", marker='s', markerfacecolor='black', markeredgecolor='grey')
    
    lines = [
        Line2D(range(1), range(1), color="white", marker='s', markerfacecolor=colors[tier], markeredgecolor='grey') for tier in range(len(alphas))
    ]
    fig.legend(lines,
               [f'$p = {a}$' for a in alphas],
               numpoints=1,
               loc='center',
               bbox_to_anchor=(.9, .5),
               fancybox=True,
               shadow=True, ncol=1)
    
    return fig

    
def signifiance_map(p_values, xlabels, ylabels=None, var=''):

    if ylabels == None:
        ylabels = xlabels
        
    cmap=plt.cm.inferno
    
    bitmap = np.vstack(np.array(list(p_values)))
    minval = round(np.min(bitmap), 2)
    maxval = round(np.max(bitmap), 2)
    print(np.sum(bitmap)/(len(bitmap)**2))
    fig, ax = plt.subplots()

    ax.xaxis.set_ticks_position('none')
    ax.set_xticks(range(len(xlabels)))
    ax.set_xticks(np.arange(.5, len(xlabels)+.5), minor=True)
    ax.set_xticklabels(xlabels, rotation=0,fontsize=7)
    
    ax.yaxis.set_ticks_position('none')
    ax.set_yticks(range(len(ylabels)))
    ax.set_yticks(np.arange(.5, len(ylabels)+.5), minor=True)
    ax.set_yticklabels(ylabels, rotation=0, fontsize=7)
    
    ax.grid(which='minor',
            color='white',
            linewidth=5
           ) 
    cax = ax.imshow(bitmap, interpolation='nearest', cmap=cmap,
                    vmin=minval, vmax=maxval)
    cax.set_clim([minval, maxval])

    print(maxval, minval)
    cbar = fig.colorbar(cax, ticks=np.linspace(minval, maxval-.0001, 3))
    cbar.ax.set_yticklabels([r'$%s %0.2f$'%(var,a) for a in [minval, (minval+maxval)/2, maxval]])
    
    return fig

    
def lighten(color, amount=0.5):
    """
    Lightens the given color by multiplying (1-luminosity) by the given amount.
    Input can be matplotlib color string, hex string, or RGB tuple.

    Examples:
    >> lighten_color('g', 0.3)
    >> lighten_color('#F034A3', 0.6)
    >> lighten_color((.3,.55,.1), 0.5)
    """
    import matplotlib.colors as mc
    import colorsys
    try:
        c = mc.cnames[color]
    except:
        c = color
    c = colorsys.rgb_to_hls(*mc.to_rgb(c))
    return colorsys.hls_to_rgb(c[0], 1 - amount * (1 - c[1]), c[2])


def plot_char_embedding(embedding_matrix, char2index, caption=None, ax=None, latex_convertible=False):
    
    def latex_escape(char2index):
        char2index_escaped = {}
        for char, idx in char2index.items():
            if char == "\\":
                char2index_escaped[r'\textbackslash'] = idx
            elif char == r'~':
                char2index_escaped[r'\textasciitilde'] = idx
            elif char == r'^':
                char2index_escaped[r'\textasciicircum'] = idx
            elif char in ['&', '%', '#', '$', '_', '{', '}']:
                char2index_escaped['\\'+char] = idx
            else:
                char2index_escaped[char] = idx
        return char2index_escaped
    
    if latex_convertible:
        char2index = latex_escape(char2index)
                
    
    scaler = sklearn.preprocessing.StandardScaler()
    embedding_matrix = scaler.fit_transform(embedding_matrix)
    pca = PCA(n_components=2)
    embedding_matrix = pca.fit_transform(embedding_matrix)
    
    
    if ax is None:
        fig, ax = plt.subplots(nrows=1, ncols=1)
        fig.set_size_inches(7, 7)
    else:
        fig = ax.figure
    ax.set_aspect('equal', 'box')
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    m = 8
    ax.set_xlim(-m,m)
    ax.set_ylim(-m,m)

    
    color_a1, color_a2 = 'firebrick', 'rosybrown' 
    color_b1, color_b2 = 'mediumseagreen', 'mediumaquamarine' 
    color_c1, color_c2 = 'darkslategrey', 'lightsteelblue' 

    colors = [np.array((44, 95, 137))/255,
              np.array((45, 142, 88))/255,
              np.array((236, 0, 140))/255]
    
    color_a1 = colors[0]
    color_a2 = lighten(color_a1)
    color_b1 = colors[1]
    color_b2 = lighten(color_b1)
    color_c1 = colors[2]
    color_c2 = lighten(color_c1)
    color_d1 = colors[2]
    color_d2 = lighten(color_d1)
    
    line1 = Line2D(range(1), range(1), color="white", marker='s', markerfacecolor=color_a2, markeredgecolor=color_a1)
    line2 = Line2D(range(1), range(1), color="white", marker='s', markerfacecolor=color_b2, markeredgecolor=color_b1)
    line3 = Line2D(range(1), range(1), color="white", marker='s', markerfacecolor=color_c2, markeredgecolor=color_c1)
    line4 = Line2D(range(1), range(1), color="white", marker='s', markerfacecolor=color_d2, markeredgecolor=color_d1)


    def colors_by_class(char):
        if char.isalpha():
            return color_a1, color_a2
        if char.isdigit():
            return color_b1, color_b2
        if char == ' ':
            return color_d1, color_d2
        else:
            return color_c1, color_c2

    for char, i in char2index.items():
        edge_color, fill_color = colors_by_class(char)
        ax.annotate(char,
                    (embedding_matrix[i][0], embedding_matrix[i][1]),
                    color= edge_color,
                    ha='center', 
                    va='center',
                    size = 16)

    if caption:
        ax.set_title(caption)
        
#####################################################

import numpy as np
from numpy.random import randint
import matplotlib.pyplot as plt
from math import log
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset
from incl.embedding_utils import indices_of_most_similar

from matplotlib import rc, rcParams



def closest(vec, matrix, n=10):
    distances = (matrix - vec)**2
    distances = np.sum(distances, axis=1)
    distances = np.sqrt(distances, dtype=np.float64)
    l = zip(range(len(distances)), distances)
    l = sorted(l, key=lambda x : x[1])
    closest, _ = zip(*l[:n])
    return closest


def label_word(word, idx, ax, matrix):
    return ax.annotate(word,
                (matrix[idx][0], matrix[idx][1]), color='black', fontsize='small',
                bbox=dict(alpha=.5, facecolor=colors[2],
                          edgecolor='darkslategrey', boxstyle='round,pad=.4' ) )


def plot_words(matrix, word2index, index2word, words, fig, ax):
    plotted = []
    # get index for most similar words and plot their vectors
    index = [word2index[w] for w in words]
    plotted.append(ax.scatter(matrix[index][:, 0], matrix[index][:, 1], s=5,
               color=colors[1], label='$k$ most similar words'))
    for word in words:
        i = word2index[word]
        plotted.append(label_word(word, i, ax, matrix))
    
    return plotted 


def plot_embedding(matrix, word2index, index2word, fig=None, ax=None):
    
    if fig is None and ax is None:
        fig, ax = plt.subplots(nrows=1, ncols=1)
        
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.grid(linestyle='--')
    
    ax.scatter(matrix[:, 0], matrix[:, 1], s=1, color=colors[0], label='words') 
        
    return fig, ax


def plot_focus_word(matrix, word2index, index2word, focus_word, dist, fig, ax, axins):

    focus_vec = matrix[word2index[focus_word]]
    
    axins.yaxis.tick_right()
    axins.set_xlim(focus_vec[0]-dist, focus_vec[0]+dist)
    axins.set_ylim(focus_vec[1]-dist, focus_vec[1]+dist)
    [i.set_linewidth(.5) for i in axins.spines.values()]
    axins.grid(linestyle='--')
    
    mark_inset(ax, axins, loc1=2, loc2=3, fc="none", ec=".2", linewidth=.5)
    
    axins.scatter(matrix[:,0], matrix[:,1], s=2, color=colors[0])
    
    closest_after_reduction = closest(focus_vec, matrix, 20)
    for i in closest_after_reduction:
        word = index2word[i]
        label_word(word, i, axins, matrix)
            
    return axins

def interative_embedding_plot(embedding_matrix_reduced, embedding_matrix, word2index, index2word):
    
    from ipywidgets import widgets
    from IPython.display import display
    import numpy as np
    import matplotlib.pyplot as plt
    from matplotlib import gridspec
    from incl.embedding_utils import most_similar_words, present_most_similar
    from sklearn.metrics.pairwise import euclidean_distances
    from scipy.spatial.distance import cosine as cosdist
    import pandas as pd
    
    text = widgets.Text()
    display(text)

    fig = plt.figure(figsize=(11, 6)) 
    fig.suptitle('word embedding projected onto 2D subspace', fontsize=16, y=1.03)
    gs = gridspec.GridSpec(1, 2, width_ratios=[1, 1]) 
    ax = plt.subplot(gs[0])
    axins = plt.subplot(gs[1])
    plot_embedding(embedding_matrix_reduced, word2index, index2word, fig, ax)
    ax.set_title('$k$ nearest words in original space (cosine similarity)', y=1.03)
    axins.axis('off')

    plotted = []

    def handle(sender):
        nonlocal plotted
        nonlocal ax
        nonlocal axins
        if text.value != '':
            plt.cla()
            axins.set_title('$k$ nearest words in 2D space (euclidean distance)', y=1.03)
            for p in plotted:
                p.remove()
                del p
            try:
                msws_o = most_similar_words(text.value, embedding_matrix, word2index, index2word)
                
                msvs_r = np.array(list(
                    map(lambda w : embedding_matrix_reduced[word2index[w]], msws_o)))
                
                msvs_o = np.array(list(
                    map(lambda w : embedding_matrix[word2index[w]], msws_o)))
                ### display dataframe of distances
                l = [(msw_o, cosdist(embedding_matrix[word2index[text.value]], msv_o))
                 for msw_o, msv_o in zip(msws_o, msvs_o)]
                l = sorted(l, key=lambda x : x[1])
                df = pd.DataFrame(columns=['word', 'distance'], data=l)
                print(df.to_latex(index=False))
                display(df)
                ###
                
                dist = np.max(euclidean_distances(msvs_r))/3
                dist = 1/np.sqrt(dist)
#                 print(dist, np.log(dist))
            except KeyError:
                print(f'No entry for "{text.value}"')
            plotted = plot_words(embedding_matrix_reduced, word2index, index2word, msws_o, fig, ax)
            plot_focus_word(embedding_matrix_reduced, word2index, index2word, text.value, dist, fig, ax, axins)
            
            plt.show()

    text.on_submit(handle)
    
    return fig
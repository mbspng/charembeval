#!/usr/bin/env python

from os.path import join
from os import makedirs

embeddings_dir       = 'data/embeddings'
amazon_dir             = 'data/amazon-reviews' 
checkpoint_dir       = join(amazon_dir, 'models/checkpoints')
completed_models_dir = join(amazon_dir, 'models/completed')
training_history_dir = join(amazon_dir, 'models/histories')
hdfstores_dir        = join(amazon_dir, 'stores')
databases_dir        = join(amazon_dir, 'models/databases')
encoders_dir         = join(amazon_dir, 'models/encoders')
experiments_dir      = join(amazon_dir, 'experiments')

makedirs(embeddings_dir)
makedirs(amazon_dir)
makedirs(checkpoint_dir)
makedirs(completed_models_dir)
makedirs(training_history_dir)
makedirs(hdfstores_dir)
makedirs(databases_dir)
makedirs(encoders_dir)
makedirs(experiments_dir)

# Trains a model with the setup specified in $attributes_for_models.


from incl.utils import dotdict

def set_up(attributes):
    for key, value in attributes:
        assert key in setup, f'{key} not in setup'
        setup[key] = value
    if '::' in setup.name:
        raise Exception('double colon not allowed in model name')
    assert not any([setup[x] is None for x in ['modeltype', 'name',
                                               'char_embedding_file', 'epochs',
                                               'trainable', 'data_file',
                                               'center', 'standardize',
                                               'normalize', 'raunak',
                                               'rightsingularvectors',
                                               'original_embeddings_path']]),
                   'some variable was left as None'


num_epochs = 12
letter_data_path = 'data/amazon-reviews/stores/amazon-reviews.h5' 
phon_data_path = 'data/amazon-reviews/stores/amazon-reviews-phonetic-espeak.pkl'
original_embeddings_path = 'original_embeddings.pkl'

attributes_for_models = [
#  example
#  [('modeltype', 'non-semantic'),
#   ('comment', 'no postprocessing of embedding'),
#   ('name', 'OHP'),
#   ('char_embedding_file', 'one-hot-phone-ident.pkl'),
#   ('epochs', num_epochs),
#   ('trainable', True),
#   ('data_file', phon_data_path),
#   ('center', False),
#   ('standardize', False),
#   ('normalize', False),
#   ('raunak', False),
#   ('original_embeddings_path', original_embeddings_path),
#   ('rightsingularvectors', False)],
]

for attributes in attributes_for_models:

    print(attributes)

    # contains primary hyper parameters. secondary hyper parameters
    # will be derived.
    # also contains meta data, such as comments for the data base entry.
    # variable entries as well as the meta data entries need to be adjusted
    # for each model, while fixed entries are intended to be kept constant
    # across models.
    setup = dotdict({
        # varibale
        'modeltype' : None,
        'name' : None,
        'char_embedding_file' : None,
        'epochs' : None,
        'trainable' : None,
        'epochs' : None,
        # fixed
        'text_length' : 1014,
        'batch_size' : 128,
        'filter_cnt' : 256,
        'dense_dims' : 1024,
        'dropout_rate' : 0.5,
        'optimizer' : 'adam',
        'loss' : 'categorical_crossentropy',
        # meta
        'description_data' : '',
        'description_model' : '',
        'description_encoding'  : '',
        'comment' : '',
        # data
        'data_file' : None,
        'center' : None,
        'standardize' : None,
        'normalize' : None,
        'raunak' : None,
        'rightsingularvectors' : None,
        'original_embeddings_path' : None 
    })


        
    set_up(attributes)

    # fixed pathes
    
    import time
    from datetime import datetime
    from os.path import join

    # set up directories

    embeddings_dir       = 'data/embeddings'
    base_dir             = 'data/amazon-reviews'
    checkpoint_dir       = join(base_dir, 'models/checkpoints')
    completed_models_dir = join(base_dir, 'models/completed')
    training_history_dir = join(base_dir, 'models/histories')
    hdfstores_dir        = join(base_dir, 'stores')
    databases_dir        = join(base_dir, 'models/databases')
    encoders_dir         = join(base_dir, 'models/encoders')

    # define file pathes

    sql_database_path = join(databases_dir, 'models.db')
    hdf_database_path = join(databases_dir, 'models.h5')

    # variable pathes
    
    name = setup.name

    timestamp = datetime.utcfromtimestamp(time.time()).strftime('%d.%m.%Y_%H:%M:%S')
    model_name = f'{name}::{timestamp}'

    model_path = join(completed_models_dir, model_name)
    checkpoint_path = join(checkpoint_dir, model_name + '-ep_{epoch:d}-va_{val_acc:.4f}.h5')

    history_figure_path_pdf = join(training_history_dir, model_name + '.pdf')
    history_figure_path_png = join(training_history_dir, model_name + '.png')
    history_pickle_path = join(training_history_dir, model_name + '.pkl')

    X_encoder_pickle_path = join(encoders_dir, model_name + '-X_encoder.pkl')
    Y_encoder_pickle_path = join(encoders_dir, model_name + '-Y_encoder.pkl')

    char_embedding_path = join(embeddings_dir, setup.char_embedding_file)

    print(model_name)

    from incl.embedding_utils import load_embedding_from_pickle
    from incl.encode_decode import text2indices
    from sklearn import preprocessing
    from sklearn.decomposition import PCA
    import numpy as np
    from scipy.linalg import orth
    from incl.stats_utils import raunaks_algorithm
    from sklearn.utils.extmath import randomized_svd

    embedding_matrix, char2index, index2char = load_embedding_from_pickle(char_embedding_path)


    if setup.center:
        print('Centering embedding.')
        scaler = preprocessing.StandardScaler(with_std=False)
        embedding_matrix = scaler.fit_transform(embedding_matrix)
        
    if setup.standardize:
        print('Standardizing embedding.')
        scaler = preprocessing.StandardScaler()
        embedding_matrix = scaler.fit_transform(embedding_matrix)

    if setup.normalize:
        print('Normalizing embedding.')
        embedding_matrix = preprocessing.normalize(embedding_matrix, axis=1)
        
    if setup.raunak:
        print("Applying Raunak's algoritm to embedding.")
        print('embedding dimensions:', embedding_matrix.shape)
        n1 = min(embedding_matrix.shape)
        print('n1', n1)
        n2 = n1//2
        embedding_matrix = raunaks_algorithm(embedding_matrix, n1, n2)
        encoding_dims = n2
        print('n2', n2)
        assert(n2 == embedding_matrix.shape[1])

    if setup.rightsingularvectors:
        print('Finding right singular vectors of embedding.')
        _, _, embedding_matrix = randomized_svd(embedding_matrix,
                                                n_components=len(embedding_matrix),
                                                n_iter=5,
                                                random_state=None)

    def X_encoder(text):
        return text2indices(text=text, char2index=char2index, text_length=setup.text_length)

    from incl.plotting import plot_char_embedding

    # encoding of categories to one hot vectors

    from incl.encode_decode import cat2onehot

    def Y_encoder(categories):
        return cat2onehot(categories=categories,
                          num_categories=len(id2cat))

    import pandas as pd
    import pickle
    
    print('Loading training data.')
    try:
        with open(setup.data_file, 'rb') as f:
            data = pickle.load(f)
    except:
        store = pd.HDFStore(setup.data_file)
        data = store.data
        
    import random

    train_cut, val_cut = .8, .9
    t, v = train_cut, val_cut
    assert(t + (v-t) + (1-v) == 1)

    num_samples = len(data)

    indices = list(range(num_samples))

    train_indices = indices[0:int(t*num_samples)]
    val_indices = indices[int(t*num_samples):int(v*num_samples)]
    test_indices = indices[int(v*num_samples):num_samples]

    train = data.iloc[train_indices]
    val = data.iloc[val_indices]
    test = data.iloc[test_indices]

    id2cat = dict(enumerate(data.category.cat.categories))
    categories = list(id2cat.values())

    from incl.AmazonReviewDataGenerator import AmazonReviewDataGenerator
    from incl.AmazonReviewAugmentedDataGenerator import AmazonReviewAugmentedDataGenerator

    DataGenerator = AmazonReviewDataGenerator
    # DataGenerator = AmazonReviewAugmentedDataGenerator

    train_gen = DataGenerator(train,
                              text_length=setup.text_length,
                              X_encoder=X_encoder,
                              Y_encoder=Y_encoder,
                              batch_size=setup.batch_size)

    val_gen = DataGenerator(val,
                            text_length=setup.text_length,
                            X_encoder=X_encoder,
                            Y_encoder=Y_encoder,
                            batch_size=setup.batch_size)

    test_gen = DataGenerator(test,
                             text_length=setup.text_length,
                             X_encoder=X_encoder,
                             Y_encoder=Y_encoder,
                             batch_size=setup.batch_size,
                             shuffle=False)

    # architecture parameters (all following ZLL)
    filter_cnt = setup.filter_cnt # number of stacked convolutions
    dense_dims = setup.dense_dims  # dimensonality of dense internal layers
    dropout_rate = setup.dropout_rate # dropout rate
    output_dims = len(id2cat)  # dimensionality for dense output layer
    # training parameters
    optimizer = setup.optimizer
    loss = setup.loss
    epochs = setup.epochs

    from keras.models import Sequential
    from keras.layers import Flatten, Dense, Conv1D, MaxPool1D, Dropout, Embedding
    from keras.optimizers import SGD
    from keras.initializers import glorot_normal
    from keras.utils import multi_gpu_model

    model = Sequential()

    vocab_size = embedding_matrix.shape[0]
    print(vocab_size)
    encoding_dims = embedding_matrix.shape[1]
    print(encoding_dims)

    embedding_layer = Embedding(vocab_size, encoding_dims, input_length=setup.text_length,
                                trainable=setup.trainable, name='emb')
    embedding_layer.build((None,))
    embedding_layer.set_weights([embedding_matrix])
    model.add(embedding_layer)
    print(setup.trainable)

    model.add(Conv1D(filters=filter_cnt, kernel_size=7, activation='relu', input_shape=(setup.text_length, encoding_dims), bias_initializer=glorot_normal()))

    model.add(MaxPool1D(pool_size=3, strides=3))

    model.add(Conv1D(filters=filter_cnt, kernel_size=7, activation='relu', bias_initializer=glorot_normal()))

    model.add(MaxPool1D(pool_size=3, strides=3))

    model.add(Conv1D(filters=filter_cnt, kernel_size=3, activation='relu', bias_initializer=glorot_normal()))

    model.add(Conv1D(filters=filter_cnt, kernel_size=3, activation='relu', bias_initializer=glorot_normal()))

    model.add(Conv1D(filters=filter_cnt, kernel_size=3, activation='relu', bias_initializer=glorot_normal()))

    model.add(Conv1D(filters=filter_cnt, kernel_size=3, activation='relu', bias_initializer=glorot_normal()))

    model.add(MaxPool1D(pool_size=3, strides=3))

    model.add(Flatten())

    model.add(Dense(units=dense_dims, activation='relu', bias_initializer=glorot_normal()))

    model.add(Dropout(rate=dropout_rate))

    model.add(Dense(units=dense_dims, activation='relu', bias_initializer=glorot_normal()))

    model.add(Dropout(rate=dropout_rate))

    model.add(Dense(units=output_dims, activation='softmax', bias_initializer=glorot_normal()))

#     try:
#         model_mpgu = multi_gpu_model(model, gpus=8)
#         print('multi gpu model active')
#     except Exception as e:
#         print('single gpu model active')

    model.compile(loss=loss, optimizer=optimizer, metrics=['accuracy'])
#     model_mgpu.compile(loss=loss, optimizer=optimizer, metrics=['accuracy'])
    model.summary()

    from incl.BatchHistory import BatchHistory

    history = BatchHistory()

    from incl.keras_utils import train_model

    train_model(model=model,
                training_generator=train_gen,
                checkpoint_path=checkpoint_path,
                history=history,
                epochs=epochs,
                validation_generator=val_gen)

    logs = history.logs

    from incl.plotting import plot_history

    fig = plot_history(history, caption=name)

    # store model and parameter related information in database

    from os.path import isfile
    from incl.database_utils import store_model_to_sql_database, store_model_to_hdf_database
    import pickle
    from incl.BatchHistory import BatchHistory

    record = {
        'name' : name,
        'time_stamp' : timestamp,
        'model_name' : model_name,
        'model_path' : model_path,
        'checkpoint_path' : checkpoint_path,
        'optimizer' : optimizer,
        'loss' : loss,
        'num_epochs_max' : epochs,
        'num_epochs_actual' : logs['epoch_cnt'],
        'batch_size' : setup.batch_size,
        'alphabet' : ''.join(list(char2index.keys())),
        'text_length' : setup.text_length,
        'categories' : ' '.join(categories),
        'acc' : np.average(logs['acc'][-(int(len(logs['acc'])*.05)):]).item(),
        'val_accs' : ' '.join(map(str, logs['val_acc'])),
        'max_val_acc' : max(logs['val_acc']),
        'max_val_acc_idx' : np.argmax(logs['val_acc']).item(),
        'history_figure_path_pdf' : history_figure_path_pdf,
        'history_figure_path_png' : history_figure_path_png,
        'history_pickle_path' : history_pickle_path,
        'filter_cnt' : filter_cnt,
        'dense_dims' : dense_dims,
        'dropout_rate' : dropout_rate,
        'output_dims' : output_dims,
        'num_training_items' : len(data),
        'comment' : setup.comment,
        'description_data' : setup.descrpition_data,
        'description_model' : setup.descrpition_model,
        'description_encoding' : setup.descrpition_encoding,
        'training_time' : logs['time_elapsed'],
        'encoding_dims' : encoding_dims,
        'embedding_layer_trainable' : setup.trainable,
        'char_embedding_path' : char_embedding_path,
        'DataGenerator_type' : type(train_gen).__name__,
        'center' : setup.center,
        'normalized' : setup.normalize,
        'standardized' : setup.standardize,
        'raunak' : setup.raunak,
        'rightsingularvectors' : setup.rightsingularvectors,
        'modeltype' : setup.modeltype,
        'X_encoder_pickle_path' : X_encoder_pickle_path,
        'Y_encoder_pickle_path' : Y_encoder_pickle_path,
        'train_idx' : None,
        'test_idx' : None,
        'val_idx' : None,
        'brier_score' : None,
        'f1_score' : None,
        'precision' : None,
        'recall' : None,
        'report_str' : None,
        'fold' : None,
        'data_path' : setup.data_file
    }

    raise_excpetion_if_exists = False

    if isfile(history_figure_path_pdf) and raise_excpetion_if_exists:
        raise FileExistsError(f'File exists: {history_figure_path_pdf}')
    fig.savefig(history_figure_path_pdf, bbox_inches='tight')

    if isfile(history_figure_path_png) and raise_excpetion_if_exists:
        raise FileExistsError(f'File exists: {history_figure_path_png}')
    fig.savefig(history_figure_path_png, bbox_inches='tight')

    if isfile(history_pickle_path) and raise_excpetion_if_exists:
        raise FileExistsError(f'File exists: {history_pickle_path}')
    with open(history_pickle_path, 'wb') as f:
        pickle.dump(history, f)

    if isfile(X_encoder_pickle_path) and raise_excpetion_if_exists:
        raise FileExistsError(f'File exists: {X_encoder_pickle_path}')
    with open(X_encoder_pickle_path, 'wb') as f:
        pickle.dump(X_encoder, f)

    if isfile(Y_encoder_pickle_path) and raise_excpetion_if_exists:
        raise FileExistsError(f'File exists: {Y_encoder_pickle_path}')
    with open(Y_encoder_pickle_path, 'wb') as f:
        pickle.dump(Y_encoder, f)

    if isfile(model_path) and raise_excpetion_if_exists:
        raise FileExistsError(f'File exists: {model_path}')
    model.save(model_path)

    store_model_to_sql_database(sql_database_path, record)

    try:
        store.close()
    except:
        pass
    
    import pickle
        
    try:
        with open(setup.original_embeddings_path, 'rb') as f:
            original_embeddings = pickle.load(f)
    except:
        original_embeddings = {}
    original_embeddings[setup.name] = (embedding_matrix, char2index, index2char)
    with open(setup.original_embeddings_path, 'wb') as f:
        pickle.dump(original_embeddings, f)
        print('Wrote to embeddings file.')